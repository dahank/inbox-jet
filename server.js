const app = require('./src');

const { Config } = require('./config')

const { PORT } = Config.APPLICATION;

const onListening = () => console.log(`Inbox-Jet service is running on port : ${PORT} !!!!`);

app.listen(PORT, onListening());

module.exports = app;
